# scipy & numpy
# a docker image that includes scipy and numpy for python 3.4
# VERSION       2.0

FROM python:3.5
MAINTAINER andrew <andrew@pdview.com.au>

COPY llvm.list /etc/apt/sources.list.d/

RUN apt-get update && apt-get install --yes --force-yes \
build-essential \
libxml2-dev \
libxslt-dev \
apt-utils \
cython3 \
cython \
gfortran \
libblas-dev \
liblapack-dev \
python3-numpy \
python3-scipy \
python3-numexpr \
libpq-dev \
python3-pandas \
libsuitesparse-dev \
swig \
llvm-3.8-dev \
&& rm -rf /var/lib/apt/lists/*

ENV LLVM_CONFIG /usr/bin/llvm-config-3.8
RUN pip3 install numpy==1.11.1 scipy==0.18.0 && pip3 install llvmlite==0.13.0 && pip3 install numba==0.27.0

CMD ["python3"]
